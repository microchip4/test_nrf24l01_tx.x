/* 
 * File:   my_delays.h
 * Author: pic18fxx.blogspot.com
 * 
 */

#ifndef MY_DELAYS_H
#define	MY_DELAYS_H

void Delay_ms(unsigned int count);
void Delay_us(unsigned int count);

#endif	/* MY_DELAYS_H */